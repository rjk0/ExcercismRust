use std::collections::HashMap;
use std::thread;

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    let counter = |input: String| {
        let mut map = HashMap::new();
        input
            .chars()
            .filter(|c| c.is_alphabetic())
            .map(|c| c.to_ascii_lowercase())
            .for_each(|c| {
                *map.entry(c).or_insert(0) += 1;
            });
        map
    };
    let mut handles = Vec::with_capacity(worker_count);

    let thread_input = input.join("").clone();
    let input_size = thread_input.len() / worker_count + 1;

    (0..worker_count).for_each(|n| {
        let thread_chunk: String = thread_input
            .chars()
            .into_iter()
            .skip(input_size * n)
            .take(input_size)
            .collect();
        handles.push(thread::spawn(move || counter(thread_chunk)));
    });

    let mut map = handles.pop().unwrap().join().unwrap();
    for res in handles {
        res.join().unwrap().into_iter().for_each(|(k, v)| {
            *map.entry(k).or_default() += v;
        });
    }
    map
}
