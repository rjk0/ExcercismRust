extern crate unicode_segmentation;
use unicode_segmentation::UnicodeSegmentation;

pub fn reverse(input: &str) -> String {
    let gnirts = input.graphemes(true).rev().collect();
    gnirts
}
