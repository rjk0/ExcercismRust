use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::collections::HashMap;
use std::collections::HashSet;

pub struct CharInfo {
    occurrences_number: i8,
    positions: Vec<bool>,
}

pub struct Word {
    hash_map: HashMap<char, CharInfo>,
}

impl Word {
    pub fn new(word: &str) -> Self {
        let mut temp_hash_map: HashMap<char, CharInfo> = HashMap::new();

        for (i, c) in word.chars().enumerate() {
            match temp_hash_map.entry(c) {
                Vacant(entry) => entry.insert(CharInfo {
                    occurrences_number: 1,
                    positions: vec![false; 5],
                }),
                Occupied(entry) => entry.into_mut(),
            };
            //temp_hash_map.insert(c,temp_char_info);
        }

        Word {
            hash_map: temp_hash_map,
        }
    }
}

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&str]) -> HashSet<&'a str> {
    unimplemented!(
        "For the '{}' word find anagrams among the following words: {:?}",
        word,
        possible_anagrams
    );
}
