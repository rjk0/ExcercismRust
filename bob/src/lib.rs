pub fn reply(message: &str) -> &str {
    let msg = message.trim();
    let is_question = msg.ends_with("?");
    let is_upper = msg.to_uppercase().eq(msg);
    let has_letters = msg.chars().any(|ch| ch.is_alphabetic());
    let is_empty = msg.is_empty();

    match (is_question, is_upper, has_letters, is_empty) {
        (true, true, true, false) => "Calm down, I know what I'm doing!",
        (true, _, _, false) => "Sure.",
        (_, _, false, true) => "Fine. Be that way!",
        (false, true, true, false) => "Whoa, chill out!",
        (_, _, _, _) => "Whatever.",
    }
}
