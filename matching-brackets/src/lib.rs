struct Stack {
    stack: Vec<char>,
}

impl Stack {
    fn new() -> Self {
        Stack { stack: Vec::new() }
    }
    fn is_empty(&self) -> bool {
        self.stack.is_empty()
    }

    fn push(&mut self, ch: char) {
        self.stack.push(ch);
    }

    fn pop(&mut self, ch: char) -> bool {
        if let Some(x) = self.stack.pop() {
            match (x, ch) {
                ('(', ')') => true,
                ('[', ']') => true,
                ('{', '}') => true,
                (_, _) => false,
            }
        } else {
            false
        }
    }

    pub fn next_bracket(&mut self, ch: char) -> bool {
        match ch {
            '(' | '[' | '{' => {
                &self.push(ch);
                true
            }
            ')' | ']' | '}' => {
                self.pop(ch)
            }

            _ => true,
        }
    }
}

pub fn brackets_are_balanced(string: &str) -> bool {
    let mut stack = Stack::new();

    for c in string.chars() {
        if false == stack.next_bracket(c) {
            return false;
        }
    }
    stack.is_empty()
}
