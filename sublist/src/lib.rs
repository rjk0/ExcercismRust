//! # Sublist
//! A library with one function. But what is the function. Just WOW!!!
//! It check if one of tyhe string is sublist of the another one.
//! Can You ever imagine that?
//! # Example
//! ```
//! # use sublist::{sublist, Comparison};
//!     assert_eq!(Comparison::Sublist, sublist(&[1, 2, 3], &[1, 2, 3, 4, 5]));
//! ```
//!
//! # Example 
//! ``` 
//!use sublist::{sublist, Comparison};
//!
//!fn test_compare_larger_equal_lists() {
//!    use std::iter::repeat;
//!
//!    let v: Vec<char> = repeat('x').take(1000).collect();
//!
//!    assert_eq!(Comparison::Equal, sublist(&v, &v));
//!}
//!```
///
/// # Return struct
/// The struct that function _sublist_ returns
///
#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

/// 
/// # Our great function
/// It works like a dream
///
pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    match (_first_list.len(), _second_list.len()) {
        (0, 0) => Comparison::Equal,
        (0, _) => Comparison::Sublist,
        (_, 0) => Comparison::Superlist,
        (_first_list_len, _second_list_len) if _first_list_len > _second_list_len => {
            if _first_list.windows(_second_list_len).any(|x| x == _second_list) {
                Comparison::Superlist
            } else {
                Comparison::Unequal
            }
        }
        (_first_list_len, _second_list_len) if _first_list_len < _second_list_len => {
            if _second_list.windows(_first_list_len).any(|x| x == _first_list) {
                Comparison::Sublist
            } else {
                Comparison::Unequal
            }
        }
        (_first_list_len, _second_list_len) if _first_list_len == _second_list_len => {
            if _first_list == _second_list {
                Comparison::Equal
            } else {
                Comparison::Unequal
            }
        }

        (_, _) => Comparison::Unequal,
    }
    // unimplemented!("Determine if the first list is equal to, sublist of, superlist of or unequal to the second list.");
}
