use std::collections::HashSet;

pub fn sort(word: &str) -> Vec<char> {
    let mut sorted_word: Vec<char> = word.chars().collect();
    sorted_word.sort_unstable();
    sorted_word
}

pub fn is_anagram(
    word: &str,
    word_lowercase: &mut String,
    word_lowercase_sorted: &mut Vec<char>,
    possible_anagram: &str,
) -> bool {
    if word.len() != possible_anagram.len() {
        return false;
    }
    if word_lowercase.is_empty()  {
        *word_lowercase = word.to_lowercase();
        *word_lowercase_sorted = sort(&word_lowercase);
    }

    let possible_anagram_lowercase = possible_anagram.to_lowercase();
    if possible_anagram_lowercase == *word_lowercase {
        return false
    }
    *word_lowercase_sorted == sort(&possible_anagram_lowercase)
}

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let mut word_lowercase: String = "".to_string();
    let mut word_lowercase_sorted: Vec<char> = sort(&word_lowercase);

    possible_anagrams
        .iter()
        .cloned()
        .filter(|&possible_anagram| {
            is_anagram(word, &mut word_lowercase, &mut word_lowercase_sorted, possible_anagram)
        })
        .collect::<HashSet<&str>>()
}
