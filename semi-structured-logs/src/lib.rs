// This stub file contains items which aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

/// various log levels
#[derive(Clone, PartialEq, Debug)]
pub enum LogLevel {
    Info,
    Warning,
    Error,
    Debug,
}
/// primary function for emitting logs
pub fn log(level: LogLevel, message: &str) -> String {
    match level {
        LogLevel::Info      => info(message),
        LogLevel::Warning   => warn(message),
        LogLevel::Error     => error(message),
        LogLevel::Debug     => debug(message),
    }
}
pub fn info(message: &str) -> String {
    let mut log = String::from("[INFO]: ");
    log.push_str(message);
    log
}
pub fn warn(message: &str) -> String {
    let mut log = String::from("[WARNING]: ");
    log.push_str(message);
    log
}
pub fn error(message: &str) -> String {
    let mut log = String::from("[ERROR]: ");
    log.push_str(message);
    log
}
pub fn debug(message: &str) -> String {
    let mut log = String::from("[DEBUG]: ");
    log.push_str(message);
    log
}
