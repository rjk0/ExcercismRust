
pub fn build_proverb(list: &[&str]) -> String {
    if list.len() == 0 {
        return String::new();
    }

    list.windows(2)
        .map(|sublist| format!("For want of a {} the {} was lost.\n",sublist[0], sublist[1]))
        .chain(std::iter::once(format!("And all for the want of a {}.", list[0])))
        .collect()

}
