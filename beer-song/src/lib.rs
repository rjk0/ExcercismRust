//use std::borrow::Cow;

pub fn verse(n: u32) -> String {
    let mut line1 = match n {
        1 => "1 bottle of beer on the wall, 1 bottle of beer.\n".to_string(),
        0 => "No more bottles of beer on the wall, no more bottles of beer.\n".to_string(),
        _ => format!(
            "{} bottles of beer on the wall, {} bottles of beer.\n",
            n, n
        ),
    };

    let line2 = match n {
        2 => "Take one down and pass it around, 1 bottle of beer on the wall.\n".to_string(),
        1 => "Take it down and pass it around, no more bottles of beer on the wall.\n".to_string(),
        0 => "Go to the store and buy some more, 99 bottles of beer on the wall.\n".to_string(),
        _ => format!(
            "Take one down and pass it around, {} bottles of beer on the wall.\n",
            n - 1
        ),
    };

    line1.push_str(&line2);
    line1
}

pub fn sing(start: u32, end: u32) -> String {
    //    unimplemented!("sing verses {} to {}, inclusive", start, end)
    let mut song = String::new();

    for x in (end..start + 1).rev() {
        song = song + &verse(x);
        if x != end {
            song.push('\n');
        }
    }
    song
}
