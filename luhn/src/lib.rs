/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    code.chars()
        .filter(|c| !c.is_ascii_whitespace())
        .rev()
        .try_fold((0, 0), |(sum, cnt), c| {
            c.to_digit(10).map(|n| {
                if cnt % 2 == 1 {
                    if n < 5 {
                        (sum + n * 2, cnt + 1)
                    } else {
                        (sum + n * 2 - 9, cnt + 1)
                    }
                } else {
                    (sum + n, cnt + 1)
                }
            })
        })
        .map_or(false, |(sum, cnt)| sum % 10 == 0 && cnt > 1)
}
