pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    let mut used_multiplies = vec![false; limit as usize];
    let mut sum_of_multiplies = 0;
    let mut i = 1;
    let mut j = 0;
    let mut current_factor;





    while j < factors.len() {

        current_factor = factors[j] * i;
        while current_factor < limit && factors[j] != 0 {
            if !used_multiplies[current_factor as usize] {
                sum_of_multiplies += current_factor;
                used_multiplies[current_factor as usize] = true;
            }
            i = i + 1;
            current_factor = factors[j] * i;
        }
        i = 1;
        j = j + 1;
    }
    sum_of_multiplies
}
