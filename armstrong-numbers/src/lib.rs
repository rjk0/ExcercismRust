pub fn is_armstrong_number(num: u32) -> bool {
    num == num
        .to_string()
        .chars()
        .map(|d| {
            d.to_digit(10)
                .unwrap()
                .pow(((num as f64).log10() + 1.).floor() as u32)
        })
        .sum()
}
