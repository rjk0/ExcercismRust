pub fn nth(n: u32) -> u32 {
    let mut primes = vec![2];
    let mut number = 1u32;
    'outer: loop {
       if primes.len() - 1 == n as usize {
            break;
        }
        number += 1;
        for x in &primes {
            if number % x == 0 {
                continue 'outer;
            }
        }
        primes.push(number);
    }
    primes[primes.len() - 1]
}
