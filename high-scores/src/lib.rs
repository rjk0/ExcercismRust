#[derive(Debug)]
pub struct HighScores<'a> {
    scores: &'a [u32],
}

impl<'a> HighScores<'a> {
    pub fn new(scores: &'a [u32]) -> Self {
        HighScores { scores: scores }
    }

    pub fn scores(&self) -> &[u32] {
        self.scores
    }

    pub fn latest(&self) -> Option<u32> {
        if !self.scores.is_empty() {
            Some(self.scores[self.scores.len() - 1])
        } else {
            None
        }
    }

    pub fn personal_best(&self) -> Option<u32> {
        self.scores.iter().max().map(|x| x.clone())
    }

    pub fn personal_top_three(&self) -> Vec<u32> {
        if !self.scores.is_empty() {
            let mut scores_vec: Vec<u32> = self.scores.to_vec();
            scores_vec.sort_by(|a, b| b.cmp(a));
            scores_vec.iter().take(3).cloned().collect()
        } else {
            Vec::new()
        }
    }
}
