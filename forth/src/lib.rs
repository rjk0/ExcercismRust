//use std::{collections::HashMap, error::Error};
use std::collections::HashMap;

pub type Value = i32;
pub type Result = std::result::Result<(), Error>;

#[derive(Default)]
pub struct Forth {
    stack: Vec<Value>,
    word_def: HashMap<String, String>, // ToDo: change to &str
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    DivisionByZero,
    StackUnderflow,
    UnknownWord,
    InvalidWord,
}

impl Forth {
    pub fn new() -> Forth {
        Forth {
            stack: Vec::new(),
            word_def: HashMap::new(),
        }
    }

    pub fn stack(&self) -> &[Value] {
        &self.stack
    }

    pub fn pop(&mut self) -> std::result::Result<i32, Error> {
        self.stack.pop().ok_or(Error::StackUnderflow)
    }

    pub fn push(&mut self, value: Value) -> Result {
        self.stack.push(value);
        Ok(())
    }

    pub fn process_token(&mut self, token: &str) -> Result {
        match token {
            _ if self.word_def.contains_key(token) => {
                self.eval(self.word_def.get(token).cloned().unwrap().as_str())?
            }
            "+" => self.sum()?,
            "-" => self.diff()?,
            "*" => self.prod()?,
            "/" => self.quot()?,
            "dup" => self.dup()?,
            "drop" => self.drop()?,
            "swap" => self.swap()?,
            "over" => self.over()?,
            _ if token.parse::<Value>().is_ok() => self.push(token.parse::<Value>().unwrap())?,
            _ => return Err(Error::UnknownWord),
            //            _ => {
            //                let Some(def) = self.word_def.get(token).cloned() else {
            //                    return Err(Error::UnknownWord);
            //                };
            //                println!("\t\t\t\t\t def: {}", def.as_str());
            //                self.eval(def.as_str())?
            //            }
        };
        Ok(())
    }

    pub fn define_token(&mut self, token: &str) -> Result {
        let mut chars = token.chars();
        match (chars.next(), chars.next_back()) {
            (Some(':'), Some(';')) => {}
            _ => return Err(Error::InvalidWord),
        }
        let (new_token, definition) = chars.as_str().trim().split_once(' ').unwrap_or(("", ""));

        if new_token.parse::<Value>().is_ok() {
            return Err(Error::InvalidWord);
        }

        let mut buf = String::new();

        for word in definition.split_whitespace() {
            if self.word_def.contains_key(word) {
                buf.push_str(self.word_def.get(word).unwrap().as_str());
            } else {
                buf.push_str(word);
            }
            buf.push(' ');
        }

        //        if self.word_def.contains_key(definition) {
        //            definition = self.word_def.get(definition).unwrap().as_str();
        //        }

        //        self.word_def
        //            .insert(new_token.to_string(), definition.to_string());

        self.word_def.insert(new_token.to_string(), buf);

        Ok(())
    }

    pub fn eval(&mut self, input: &str) -> Result {
        let v: Vec<_> = input.rmatch_indices(|c| [':', ';'].contains(&c)).collect();
        let mut input_splited: Vec<&str> = vec![];
        let mut buf = input;

        v.into_iter().for_each(|pos| {
            let (first, last) = if buf.chars().nth(pos.0) == Some(';') {
                buf.split_at(pos.0 + 1)
            } else {
                buf.split_at(pos.0)
            };
            buf = first;
            input_splited.insert(0, last.trim());
        });
        input_splited.insert(0, buf.trim());

        for expression in input_splited {
            if expression.chars().nth(0) != Some(':') {
                expression
                    .to_lowercase()
                    .split_whitespace()
                    .try_for_each(|token| self.process_token(token))?;
            } else {
                self.define_token(expression.to_lowercase().as_str())?;
            }
        }
        Ok(())
    }

    pub fn sum(&mut self) -> Result {
        let r1 = self.pop()?;
        let r2 = self.pop()?;
        self.push(r1 + r2)?;
        Ok(())
    }

    pub fn diff(&mut self) -> Result {
        let r1 = self.pop()?;
        let r2 = self.pop()?;
        self.push(r2 - r1)?;
        Ok(())
    }

    pub fn prod(&mut self) -> Result {
        let r1 = self.pop()?;
        let r2 = self.pop()?;
        self.push(r1 * r2)?;
        Ok(())
    }

    pub fn quot(&mut self) -> Result {
        let r1 = self.pop()?;
        let r2 = self.pop()?;

        match r1 {
            0 => Err(Error::DivisionByZero),
            _ => self.push(r2 / r1),
        }
    }

    pub fn dup(&mut self) -> Result {
        let r;
        match self.stack().last() {
            Some(x) => r = *x,
            None => return Err(Error::StackUnderflow),
        }
        self.push(r) //?;
    }

    pub fn drop(&mut self) -> Result {
        self.pop()?;
        Ok(())
    }
    pub fn swap(&mut self) -> Result {
        let r1 = self.pop()?;
        let r2 = self.pop()?;
        self.push(r1)?;
        self.push(r2)?;
        Ok(())
    }
    pub fn over(&mut self) -> Result {
        let r1 = self.pop()?;
        let r2 = self.pop()?;

        self.push(r2)?;
        self.push(r1)?;
        self.push(r2)?;
        Ok(())
    }
}
