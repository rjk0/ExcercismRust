// This stub file contains items which aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

use std::collections::HashMap;

pub fn can_construct_note(magazine: &[&str], note: &[&str]) -> bool {
    // let mut words_map : HashMap<&str, u8> = HashMap::new();

    let mut words_map = magazine.iter().fold(HashMap::new(), |mut acc, x| {
        *acc.entry(x).or_insert(0) += 1;
        acc
    });

    note.iter()
        .fold(words_map, |mut acc, x| {
            acc.entry(x).and_modify(|e| *e -= 1).or_insert(-1);
            acc
        })
        .into_iter()
        .all(|(key, value)| value >= 0)

}
