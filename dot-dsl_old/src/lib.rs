pub mod graph {
    use graph_items::{edge::Edge, node::Node};
    use std::collections::hash_map::HashMap;

    pub struct Graph {
        pub nodes: Vec<Node>,
        pub edges: Vec<Edge>,
        pub attrs: HashMap<String, String>,
    }

    impl<'a> Graph {
        pub fn new() -> Self {
            Self {
                nodes: Vec::new(),
                edges: Vec::new(),
                attrs: HashMap::new(),
            }
        }

        pub fn with_nodes(mut self, nodes: &Vec<Node>) -> Self {
            // why need 'a
            self.nodes.extend_from_slice(nodes);
            self
        }
    }

    #[derive(Clone)]
    pub struct Attrs(HashMap<String, String>);


    pub mod graph_items {
        use super::*;

        pub mod edge {
            pub struct Edge;
        }

        pub mod node {
            use super::*;

            #[derive(Clone,Debug)]
            pub struct Node {
                name: String,
                attrs: HashMap<String, String>,
            }

            impl<'a> Node {

                pub fn new(name: &str) -> Self {
                    Self {
                        name: name.to_string(), 
                        attrs: HashMap::new(),
                    }
                }

                pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
                    for (key, value)  in attrs {
                        self.attrs.insert(key.to_string(), value.to_string());
                    }
                        self
                }
            }
        }
    }
}
