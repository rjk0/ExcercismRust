use std::cmp::Ordering;

/// Given a list of poker hands, return a list of those hands which win.
///
/// Note the type signature: this function should return _the same_ reference to
/// the winning hand(s) as were passed in, not reconstructed strings which happen to be equal.

// hand type:
// 5 - high card
// 6 - one pair
// 7 - two pairs
// 9 - three of kind
// 10 - full
// 15 - four of hand

#[derive(Debug)]
struct Hand<'a> {
    hand_str: &'a str,
    card_values: Vec<u8>,
    hand_type: u8,
}

impl<'a> Hand<'a> {
    fn new(hand_str: &'a str) -> Self {
        let mut card_values: Vec<u8> = hand_str.split(' ').map(Self::card_value).collect();
        card_values.sort_by(|a, b| b.cmp(a));

        let is_straight = Self::is_straight(hand_str);
        let is_flush = Self::is_flush(hand_str);
        let hand_type = Self::hand_type(hand_str, is_straight, is_flush);

        Self {
            hand_str,
            card_values,
            hand_type,
        }
    }

    fn card_value(card: &str) -> u8 {
        match card.chars().next().unwrap() {
            '1' => 10,
            'J' => 11,
            'Q' => 12,
            'K' => 13,
            'A' => 14,
            val => (val as i32 - 0x30) as u8,
        }
    }

    fn hand_type(hand_str: &str, is_straight: bool, is_flush: bool) -> u8 {
        let count_face_values =
            hand_str
                .split(' ')
                .map(Self::card_value)
                .fold(0_u64, |cfv: u64, cv: u8| {
                    let mut face_value: u8 = ((cfv >> ((cv as u16) * 4) as u64) & 0b00001111) as u8;
                    face_value = face_value * 2 + 1;
                    (face_value as u64) << (cv * 4) | cfv
                });

        let hand_type: u8 = (count_face_values % 15).try_into().unwrap();

        match (hand_type, is_straight, is_flush) {
            (5 | 6 | 7 | 9, false, false) => hand_type,
            (5, true, false) => 10,
            (_, false, true) => 11,
            (10, false, false) => 12,
            (1, false, false) => 13,
            (5, true, true) => 14,
            (_, _, _) => hand_type,
        }
    }

    fn is_straight(hand: &str) -> bool {
        let face_values: u32 = hand
            .split(' ')
            .map(|cv| 2_u32.pow((Self::card_value(cv) + 1) as u32))
            .sum();

        face_values == 32888 || (face_values / (face_values & (!face_values + 1))) == 0b11111
    }

    //tu chyba mozna bez sortowania sprwadic, przy pierwszym innym zwracamy false
    fn is_flush(hand: &str) -> bool {
        let mut faces: Vec<char> = hand
            .chars()
            .filter(|x| *x == 'S' || *x == 'D' || *x == 'H' || *x == 'C')
            .collect(); //sdhc
        faces.sort();
        faces[0] == faces[4]
    }

    fn poker_hands_cmp(&self, other: &Hand) -> Ordering {
        if self.hand_type != other.hand_type {
            other.hand_type.cmp(&self.hand_type)
        } else {
            match self.hand_type {
                10 | 14 => {
                    let mut tmp_self_card_values: Vec<u8> = self.card_values.clone();
                    if self.card_values[0] == 14 && other.card_values[4] == 2 {
                        tmp_self_card_values[0] = 1;
                        tmp_self_card_values.sort_by(|a, b| b.cmp(a));
                    }

                    let mut tmp_other_card_values: Vec<u8> = other.card_values.clone();
                    if other.card_values[0] == 14 && other.card_values[4] == 2 {
                        tmp_other_card_values[0] = 1;
                        tmp_other_card_values.sort_by(|a, b| b.cmp(a));
                    }

                    return tmp_other_card_values.cmp(&tmp_self_card_values);
                }
                12 | 13 => {
                    if other.card_values[2].cmp(&self.card_values[2]) != Ordering::Equal {
                        return other.card_values[2].cmp(&self.card_values[2]);
                    } else {
                        return other.card_values.cmp(&self.card_values);
                    }
                }
                _ => return other.card_values.cmp(&self.card_values),
            }
        }
    }
}

pub fn winning_hands<'a>(hands: &[&'a str]) -> Vec<&'a str> {
    let mut result: Vec<Hand> = hands.iter().map(|&h| Hand::new(h)).collect();

    result.sort_by(|a, b| a.poker_hands_cmp(b));
    result
        .iter()
        .filter(|h| h.poker_hands_cmp(&result[0]) == Ordering::Equal)
        .map(|h| h.hand_str)
        .collect()
}
