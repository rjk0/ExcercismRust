pub fn annotate(minefield: &[&str]) -> Vec<String> {
    let height = minefield.len();
    let width = match height {
        0 => 0,
        _ => minefield[0].len(),
    };

    (0..height as i8)
        .map(|y| {
            (0..width as i8)
                .map(|x| {
                    if minefield[y as usize].as_bytes()[x as usize] == b'*' {
                        String::from("*")
                    } else {
                        match (0..9)
                            .filter(|shift| *shift != 4)
                            .map(|shift| (y + shift % 3 - 1, x + (shift / 3) - 1))
                            .filter(|coord| {
                                (coord.0 >= 0 && coord.0 < height as i8)
                                    && (coord.1 >= 0 && coord.1 < width as i8)
                                    && minefield[coord.0 as usize].as_bytes()[coord.1 as usize]
                                        == b'*'
                            })
                            .count()
                        {
                            0 => String::from(' '),
                            n => n.to_string(),
                        }
                    }
                })
                .collect()
        })
        .collect::<Vec<String>>()
}
