pub fn factors(n: u64) -> Vec<u64> {
    let mut number_to_factorize = n;
    let mut prime_factors = Vec::new();
    let mut  wheel = vec![4, 2, 4, 2, 4, 6, 2, 6].into_iter().cycle();
    
    while number_to_factorize % 2 == 0 {
        prime_factors.push(2);
        number_to_factorize = number_to_factorize / 2;
        println!("2");
    }

    while number_to_factorize % 3 == 0 {
        prime_factors.push(3);
        number_to_factorize = number_to_factorize / 3;
        println!("3");
    }

    while number_to_factorize % 5 == 0 {
        prime_factors.push(5);
        number_to_factorize = number_to_factorize / 5;
        println!("5");
    }

    let mut k = 7;

    while k <= number_to_factorize {
        while number_to_factorize % k == 0 {
            prime_factors.push(k);
            number_to_factorize = number_to_factorize / k;
        }
                k = k + wheel.next().unwrap();
        println!("{}",k);
    }

    prime_factors
}
